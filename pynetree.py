import xlsxwriter
import random

class PyneTree(object):
    star = '*'
    leaf = '-'
    bark = '|'
    words = [word.strip().split('\t')[1].strip() for word in open('words.txt','r', newline='') if word.strip()]


    def __init__(self, height=10, trunk=(3,2), multiplier=4):
        self.height = height * 2
        self.trunk = trunk
        self.multiplier = multiplier
        self.filename = 'output/pynetree-{}.xlsx'

    @property
    def branches(self):
        if not hasattr(self, '_branches'):
            self._branches = ['{: ^{}}'.format(self.star, self.height)]
            self._branches.extend(['{: ^{}}'.format(self.leaf * n, self.height)
            
                for n in range(1, self.height + 1) if n % 2 != 0])
            self._branches.extend([
                '{: ^{}}'.format(self.bark * self.trunk[0], self.height)
                for n in range(1, self.trunk[1] + 1)])
            #self._branches = tuple(self._branches)
        return self._branches

    def random_colour(self, workbook):        
        colours = random.choice(['green'] * self.multiplier + ['red', 'purple', 'blue'])
        return workbook.add_format({'bg_color': colours, 'color': colours})
    
    def export(self, filename='pyne-tree.xlsx'):
        word = random.choice(self.words)
        workbook = xlsxwriter.Workbook(self.filename.format(word))
        worksheet = workbook.add_worksheet()
        worksheet.set_column(0, self.height - 2, width=3)
        
        formats = {
            self.star: workbook.add_format({'bg_color': 'yellow', 
                'color': 'yellow'}),
            self.bark: workbook.add_format({'bg_color': 'brown', 
                'color': 'brown'}),
            ' ' : None
        }

        for row, branch in enumerate(self.branches):
            for col, item in enumerate(branch):
                worksheet.write_string(row, col, item, cell_format=formats.get(item, self.random_colour(workbook)))
                worksheet.set_row(row, height=30)
        worksheet.write_string(row+1, 0, 'Happy "{}" Christmas'.format(word))

        

        workbook.close()

    def __str__(self):
        return '\n'.join(self.branches)


if __name__ == '__main__':
    for i in range(50):
        tree = PyneTree(multiplier = random.choice(range(1,11))) 
        tree.export()

