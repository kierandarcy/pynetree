# -*- coding: utf-8 -*-
from distutils.core import setup

setup(name='PyneTree',
      version='0.1',
      description='Generate a Christmas tree in Excel.',
      author='Kieran Darcy',
      author_email='kieran@darcy.email',
      py_modules=['pynetree',],
      install_requires=['XlsxWriter==0.6.4',],
     )
